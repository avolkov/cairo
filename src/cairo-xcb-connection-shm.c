/* cairo - a vector graphics library with display and print output
 *
 * Copyright © 2009 Intel Corporation
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * (the "LGPL") or, at your option, under the terms of the Mozilla
 * Public License Version 1.1 (the "MPL"). If you do not alter this
 * notice, a recipient may use your version of this file under either
 * the MPL or the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-2.1; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 * You should have received a copy of the MPL along with this library
 * in the file COPYING-MPL-1.1
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL or the MPL for
 * the specific language governing rights and limitations.
 *
 * Contributor(s):
 *	Chris Wilson <chris@chris-wilson.co.uk>
 */

#include "cairoint.h"

#if CAIRO_HAS_XCB_SHM_FUNCTIONS

#include "cairo-xcb-private.h"

#include <xcb/xcbext.h>
#include <xcb/shm.h>

#if CAIRO_HAS_XCB_SHM_FD_FUNCTIONS
#include <xcb/xcb_aux.h>
#include <sys/mman.h>
#include <unistd.h>
#endif

#include <sys/shm.h>
#include <errno.h>

cairo_int_status_t
_cairo_xcb_connection_shm_create_segment (cairo_xcb_connection_t *connection,
				  size_t size,
				  cairo_bool_t readonly,
				  cairo_xcb_shm_segment_info_t *shm,
				  cairo_bool_t checked)
{
    xcb_void_cookie_t cookie;
    xcb_generic_error_t *error;

#if CAIRO_HAS_XCB_SHM_FD_FUNCTIONS
    if (connection->flags & CAIRO_XCB_HAS_SHM_FD) {
	int fd = xcb_aux_alloc_shm(size);
	if (fd == -1)
	    return errno == ENOMEM ? CAIRO_INT_STATUS_NO_MEMORY : CAIRO_INT_STATUS_UNSUPPORTED;

	shm->shm = mmap (NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if (unlikely (shm->shm == MAP_FAILED))
	    return _cairo_error (CAIRO_INT_STATUS_NO_MEMORY);

	shm->shmseg = _cairo_xcb_connection_get_xid (connection);
	if (unlikely (checked)) {
	    cookie = xcb_shm_attach_fd_checked (connection->xcb_connection, shm->shmseg, fd, readonly);
	    error = xcb_request_check (connection->xcb_connection, cookie);
	    close (fd);
	    if (error != NULL) {
		_cairo_xcb_connection_put_xid (connection, shm->shmseg);
		munmap (shm->shm, size);
		free (error);
		return CAIRO_INT_STATUS_UNSUPPORTED;
	    }
	} else {
	    xcb_shm_attach_fd (connection->xcb_connection, shm->shmseg, fd, readonly);
	    xcb_flush (connection->xcb_connection);
	    close (fd);
	}
    } else
#endif
    {
	shm->shmid = shmget (IPC_PRIVATE, size, IPC_CREAT | 0600);
	if (shm->shmid == -1)
	    return errno == ENOMEM ? CAIRO_INT_STATUS_NO_MEMORY : CAIRO_INT_STATUS_UNSUPPORTED;

	shm->shm = shmat (shm->shmid, NULL, 0);
	shmctl (shm->shmid, IPC_RMID, NULL);
	if (unlikely (shm->shm == (char *) -1))
	    return _cairo_error (CAIRO_INT_STATUS_NO_MEMORY);

	shm->shmseg = _cairo_xcb_connection_get_xid (connection);
	if (unlikely (checked)) {
	    cookie = xcb_shm_attach_checked (connection->xcb_connection, shm->shmseg, shm->shmid, readonly);
	    error = xcb_request_check (connection->xcb_connection, cookie);
	    if (error != NULL) {
		_cairo_xcb_connection_put_xid (connection, shm->shmseg);
		shmdt (shm->shm);
		free (error);
		return CAIRO_INT_STATUS_UNSUPPORTED;
	    }
	} else {
	    xcb_shm_attach (connection->xcb_connection, shm->shmseg, shm->shmid, readonly);
	}
    }

    return CAIRO_INT_STATUS_SUCCESS;
}

void
_cairo_xcb_connection_shm_destroy_segment (cairo_xcb_connection_t *connection,
				  cairo_xcb_shm_segment_info_t *shm)
{
    xcb_shm_detach (connection->xcb_connection, shm->shmseg);
    _cairo_xcb_connection_put_xid (connection, shm->shmseg);
#if CAIRO_HAS_XCB_SHM_FD_FUNCTIONS
    if (connection->flags & CAIRO_XCB_HAS_SHM_FD)
	munmap (shm->shm, shm->size);
    else
#endif
	shmdt (shm->shm);
}

void
_cairo_xcb_connection_shm_put_image (cairo_xcb_connection_t *connection,
				     xcb_drawable_t dst,
				     xcb_gcontext_t gc,
				     uint16_t total_width,
				     uint16_t total_height,
				     int16_t src_x,
				     int16_t src_y,
				     uint16_t width,
				     uint16_t height,
				     int16_t dst_x,
				     int16_t dst_y,
				     uint8_t depth,
				     uint32_t shm,
				     uint32_t offset)
{
    assert (connection->flags & CAIRO_XCB_HAS_SHM);
    xcb_shm_put_image (connection->xcb_connection, dst, gc, total_width, total_height,
		       src_x, src_y, width, height, dst_x, dst_y, depth,
		       XCB_IMAGE_FORMAT_Z_PIXMAP, 0, shm, offset);
}

cairo_status_t
_cairo_xcb_connection_shm_get_image (cairo_xcb_connection_t *connection,
				     xcb_drawable_t src,
				     int16_t src_x,
				     int16_t src_y,
				     uint16_t width,
				     uint16_t height,
				     uint32_t shmseg,
				     uint32_t offset)
{
    xcb_shm_get_image_reply_t *reply;

    assert (connection->flags & CAIRO_XCB_HAS_SHM);
    reply = xcb_shm_get_image_reply (connection->xcb_connection,
				     xcb_shm_get_image (connection->xcb_connection,
							src,
							src_x, src_y,
							width, height,
							(uint32_t) -1,
							XCB_IMAGE_FORMAT_Z_PIXMAP,
							shmseg, offset),
				     NULL);
    free (reply);

    if (!reply) {
	/* an error here should be impossible */
	return _cairo_error (CAIRO_STATUS_READ_ERROR);
    }

    return CAIRO_STATUS_SUCCESS;
}

#endif /* CAIRO_HAS_XCB_SHM_FUNCTIONS */
